/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2021  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FreeGas.h"

#include <dlfcn.h>
#include <link.h>
#include <sys/mman.h>
#include <sys/user.h>

#include <cstring>

FreeGas g_instance;

FreeGas::FreeGas()
{
    pAddr = findMethod("_ZN8CVehicle9PreRenderEv");
    if (!pAddr) return;
    mprotect((void*)(pAddr & 0xFFFFF000), PAGE_SIZE, PROT_READ | PROT_WRITE | PROT_EXEC);
    orig = *(uintptr_t*)pAddr;
}

FreeGas::~FreeGas()
{
    if (pAddr && orig){
        mprotect((void*)(pAddr & 0xFFFFF000), PAGE_SIZE, PROT_READ | PROT_WRITE | PROT_EXEC);
        *(uintptr_t*)pAddr = orig;
    }
}

int FreeGas::CVehicle_PreRender(uintptr_t _this)
{
    *(uint8_t*)(_this + 0x42C) |= 0x10;
    return ((int (*)(uintptr_t))(g_instance.orig))(_this);
}

uintptr_t FreeGas::findMethod(const char *name)
{
    auto func_addr = dlsym(dlopen("libGTASA.so", RTLD_NOW), name);
    if (!func_addr) return 0;
    
    Dl_info info;
    dladdr(func_addr, &info);
    auto lib = (uint8_t*)info.dli_fbase;
    
    auto hdr = (Elf32_Ehdr*)lib;
	auto pht = (Elf32_Phdr*)(&lib[hdr->e_phoff]);

	Elf32_Dyn* dyn = nullptr;
	for (int i = 0; i < hdr->e_phnum; ++i, pht++) {
		if (pht->p_type != 2) continue;
		dyn = (Elf32_Dyn*)(&lib[pht->p_offset]);
		break;
	}
	if (!dyn) return 0;

	uintptr_t strtab = 0;
	uintptr_t fnctab = 0;
	uintptr_t reltab = 0;
	uintptr_t jmpreltab = 0;
	for (; dyn->d_tag != 0; dyn++) {
		if (dyn->d_tag == 5) strtab = dyn->d_un.d_val;
		else if (dyn->d_tag == 6) fnctab = dyn->d_un.d_val;
		else if (dyn->d_tag == 17) reltab = dyn->d_un.d_val;
		else if (dyn->d_tag == 23) jmpreltab = dyn->d_un.d_val;
	}
	if (!strtab || !fnctab || !reltab || !jmpreltab) return 0;

	uintptr_t index = 0;
	uintptr_t address = 0;
	auto sym = (Elf32_Sym*)(&lib[fnctab]);
	for (auto i = 0; fnctab + i * sizeof(Elf32_Sym) != strtab; ++i, sym++) {
		auto name_ptr = uintptr_t(lib) + sym->st_name + strtab;
		if (strcmp((char*)name_ptr, name)) continue;
		index = i;
        address = uintptr_t(lib) + sym->st_value;
		break;
	}
	
	struct Elf32_Plt{
        uint32_t d_value;
    };

	auto rel = (Elf32_Rel*)(&lib[reltab]);
	for (auto i = 0; i < (strtab - fnctab) / sizeof(Elf32_Sym); ++i, rel++) {
		if ((rel->r_info >> 8) != index) continue;
		return uintptr_t(lib) + rel->r_offset;
	}

	auto jmprel = (Elf32_Rel*)(&lib[jmpreltab]);
	for (auto i = 0; i < (strtab - fnctab) / sizeof(Elf32_Sym); ++i, jmprel++) {
		if ((jmprel->r_info >> 8) != index) continue;
		return uintptr_t(lib) + jmprel->r_offset;
	}
	
	return 0;
}
