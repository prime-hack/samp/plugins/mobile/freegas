/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2021  <copyright holder> <email>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FREEGAS_H
#define FREEGAS_H

#include <cstdint>

/**
 * @todo write docs
 */
class FreeGas
{
    uintptr_t orig = 0;
    uintptr_t pAddr = 0;
    
public:
    /**
     * Default constructor
     */
    FreeGas();

    /**
     * Destructor
     */
    ~FreeGas();
    
    static int CVehicle_PreRender(uintptr_t self);
    
protected:
    uintptr_t findMethod(const char *name);

};

#endif // FREEGAS_H
